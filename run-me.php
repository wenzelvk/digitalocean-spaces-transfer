<?php

// source space
$source_key        = "KEY";
$source_secret     = "SECRET";
$source_space_name = "space_name";
$source_region     = "fra1";

// target space
$target_key        = "KEY";
$target_secret     = "SECRET";
$target_space_name = "space_name";
$target_region     = "ams3";

// ------------------------------------------------- //

// show all php errors
ini_set('display_errors', 1);
error_reporting(E_ALL);
// set unlimited runtime
set_time_limit(0);
// set high memory limit to avoid errors handling large files
ini_set('memory_limit', '1024M');

// load Spaces API
require_once("vendor/sociallydev/spaces-api/spaces.php");

// prepare HTML output
echo "<pre><span id='progress'>Loading...</span><br>";
flush();

// connect to DigitalOcean Spaces
$space_source   = new SpacesConnect($source_key, $source_secret, $source_space_name, $source_region);
$space_target   = new SpacesConnect($target_key, $target_secret, $target_space_name, $target_region);

// load a list of all files from Source Space
if ( !file_exists("source-files.json") ){
    $files = json_encode($space_source->ListObjects());
    file_put_contents("source-files.json", $files);
    $files = json_decode($files, true);
}else{
    $files = json_decode(file_get_contents("source-files.json"), true);
}
$files_total = count($files);

// loop through all the source files and upload one by one
$i = 0;
foreach($files as $file){
    $file_name = $file['Key'];

    // upload if not existing
    if ($space_target->DoesObjectExist($file_name) == false){
        // download file
        $space_source->DownloadFile($file_name, 'tempfile');
        // get file permissions
        $file_permissions = $space_source->ListObjectACL($file_name);
        if ($file_permissions['Grants'][0]['Permission'] == 'READ') $file_permissions = "public"; else $file_permissions = "private";
        // get file mimetype here
        $file_info = $space_source->GetObject($file_name);
        // upload file > set permissions > set mimetype
        $space_target->UploadFile('tempfile', $file_permissions, $file_name, $file_info['ContentType']);  
    }
    $i++;
    // output progress
    ?><script>document.getElementById("progress").innerHTML = "<?php echo round(100 / $files_total * $i, 2) . "% ($i / $files_total)<br>$file_name";?>";</script><?php
    ob_end_flush(); flush(); ob_start();
}

echo "All done!";